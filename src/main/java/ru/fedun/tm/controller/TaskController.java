package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.ITaskController;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.model.Task;
import ru.fedun.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
        System.out.println("[OK]");
        System.out.println();
    }

    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        taskService.createTask(title, description);
        System.out.println("[OK]");
        System.out.println();
    }
}
