package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.IProjectController;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.model.Project;
import ru.fedun.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) {
            System.out.println(project.toString());
        }
        System.out.println("[OK]");
        System.out.println();
    }

    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println();
        System.out.println("[ENTER PROJECT TITLE]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(title, description);
        System.out.println("[OK]");
        System.out.println();
    }

    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

}
