package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void create(String title, String description) {
        Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        projectRepository.add(project);
    }

    public void create(String title) {
        Project project = new Project();
        project.setTitle(title);
        projectRepository.add(project);
    }

    public void addProject(Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    public void removeProject(Project project) {
        projectRepository.remove(project);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public void clear() {
        projectRepository.clear();
    }

}
