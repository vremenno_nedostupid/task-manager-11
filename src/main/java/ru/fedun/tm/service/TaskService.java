package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void createTask(String title) {
        Task task = new Task();
        task.setTitle(title);
        taskRepository.add(task);
    }

    public void createTask(String title, String description) {
        Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        taskRepository.add(task);
    }

    public void addTask(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    public void removeTask(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void clear() {
        taskRepository.clear();
    }

}
