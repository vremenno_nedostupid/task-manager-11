package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    public void add(Project project) {
        projects.add(project);
    }

    public void remove(Project project) {
        projects.remove(project);
    }

    public List<Project> findAll() {
        return projects;
    }

    public void clear() {
        projects.clear();
    }

}
