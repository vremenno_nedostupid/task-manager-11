package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public void add(Task task) {
        tasks.add(task);
    }

    public void remove(Task task) {
        tasks.remove(task);
    }

    public List<Task> findAll() {
        return tasks;
    }

    public void clear() {
        tasks.clear();
    }

}
