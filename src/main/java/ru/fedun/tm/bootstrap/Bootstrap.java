package ru.fedun.tm.bootstrap;

import ru.fedun.tm.api.controller.ICommandController;
import ru.fedun.tm.api.controller.IProjectController;
import ru.fedun.tm.api.controller.ITaskController;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.controller.CommandController;
import ru.fedun.tm.controller.ProjectController;
import ru.fedun.tm.controller.TaskController;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.service.CommandService;
import ru.fedun.tm.service.ProjectService;
import ru.fedun.tm.service.TaskService;
import ru.fedun.tm.util.TerminalUtil;

import static ru.fedun.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (args.length != 0) {
            runWithArgs(args[0]);
        }
        runWithCommand();
    }

    private void runWithCommand() {
        while (true) {
            switch (TerminalUtil.nextLine()) {
                case VERSION:
                    commandController.displayVersion();
                    break;
                case ABOUT:
                    commandController.displayAbout();
                    break;
                case HELP:
                    commandController.displayHelp();
                    break;
                case INFO:
                    commandController.displayInfo();
                    break;
                case COMMANDS:
                    commandController.displayCommands();
                    break;
                case ARGUMENTS:
                    commandController.displayArgs();
                    break;
                case TASK_LIST:
                    taskController.showTasks();
                    break;
                case TASK_CREATE:
                    taskController.createTask();
                    break;
                case TASK_CLEAR:
                    taskController.clearTasks();
                    break;
                case PROJECT_LIST:
                    projectController.showProjects();
                    break;
                case PROJECT_CREATE:
                    projectController.createProject();
                    break;
                case PROJECT_CLEAR:
                    projectController.clearProjects();
                    break;
                case EXIT:
                    commandController.exit();
                default:
                    commandController.displayError();
            }
        }
    }

    private void runWithArgs(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArgs();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError();
        }
    }

}
