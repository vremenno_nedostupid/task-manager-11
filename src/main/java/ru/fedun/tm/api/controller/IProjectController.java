package ru.fedun.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();
}
