package ru.fedun.tm.api.repository;

import ru.fedun.tm.model.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
