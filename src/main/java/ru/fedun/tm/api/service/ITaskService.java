package ru.fedun.tm.api.service;

import ru.fedun.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void createTask(String title);

    void createTask(String title, String description);

    void addTask(final Task task);

    void removeTask(final Task task);

    List<Task> findAll();

    void clear();

}
