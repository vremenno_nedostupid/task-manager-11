package ru.fedun.tm.api.service;

import ru.fedun.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String title, String description);

    void create(String title);

    void addProject(Project project);

    void removeProject(Project project);

    List<Project> findAll();

    void clear();

}
